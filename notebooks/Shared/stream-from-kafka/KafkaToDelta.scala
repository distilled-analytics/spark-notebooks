// Databricks notebook source
// MAGIC %md
// MAGIC # Structured Streaming, Delta, and Streaming ML 
// MAGIC > Welcome to a demo of Structured Streaming, Delta Multi-hop pipeline processing, and integrated unsupervised streaming predictions.  This notebook illustrates the concept of multi-hop delta architecture, allowing for staged branching use cases of varying levels of data granularity.
// MAGIC 
// MAGIC ### Multi-hop pipeline reasoning:
// MAGIC - Future-proofing
// MAGIC > Storing all of the raw data in the first-level "bronze" table means that at any future point, a more sophisticated use of the feature incoming set may be expanded upon / re-evaluated to gain better insights.  A re-load of all consumer tables is trivial once the new transformation logic is written and executed.
// MAGIC - Branching Access
// MAGIC > Very few ETL pipelines are created for a single purpose.  With multi-hop pipelines, a single source of truth is preserved, in real-time, with artifacts being able to be generated and consumed that are all tied directly to this source of truth.
// MAGIC - Complex Aggregations
// MAGIC > Many feature engineering data sets require incredibly complex filtering, windowing, summarized data, etc... With setting these varying levels as separate (but tied to) of the originating lower-level data sets, a single data stream can power hundreds of use cases with incredibly flexible and low-effort code.
// MAGIC 
// MAGIC > Author: Ben Wilson

// COMMAND ----------

import org.apache.spark.sql.functions._ 
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Column}
import org.apache.spark.sql.streaming._
import java.net.{InetAddress, Socket}
import scala.io.Source

// COMMAND ----------

spark.conf.set("spark.sql.shuffle.partitions", 8)

// COMMAND ----------

// MAGIC %md 
// MAGIC #### Define the schema of the json payload coming in from the Kafka stream
// MAGIC Defining this allows us to not have to parse the json as a string.  This greatly simplifies the code.

// COMMAND ----------

val schema = StructType(List(
  StructField("channel", StringType, true),
  StructField("comment", StringType, true),
  StructField("delta", IntegerType, true),
  StructField("flag", StringType, true),
  StructField("geocoding", StructType(List(
    StructField("city", StringType, true),
    StructField("country", StringType, true),
    StructField("countryCode2", StringType, true),
    StructField("countryCode3", StringType, true),
    StructField("stateProvince", StringType, true),
    StructField("latitude", DoubleType, true),
    StructField("longitude", DoubleType, true)
  )), true),
  StructField("isAnonymous", BooleanType, true),
  StructField("isNewPage", BooleanType, true),
  StructField("isRobot", BooleanType, true),
  StructField("isUnpatrolled", BooleanType, true),
  StructField("namespace", StringType, true),
  StructField("page", StringType, true),
  StructField("pageURL", StringType, true),
  StructField("timestamp", StringType, true),
  StructField("url", StringType, true),
  StructField("user", StringType, true),
  StructField("userURL", StringType, true),
  StructField("wikipediaURL", StringType, true),
  StructField("wikipedia", StringType, true)
))

// COMMAND ----------

// MAGIC %md
// MAGIC #### Establish the stream read from the Kafka server
// MAGIC - extract the timestamp from the feed that relates to the point of data entry into Kafka
// MAGIC - parse the json encoded payload and apply the schema defined above

// COMMAND ----------

val rawKafka = spark.readStream
  .format("kafka")
  .option("kafka.bootstrap.servers", "server1.databricks.training:9092")
  .option("failOnDataLoss", "false")
  .option("subscribe", "en")
  .load()
  .select($"timestamp".as("sys_timestamp"), $"value".cast("STRING").as("value"))
  .select($"sys_timestamp", from_json($"value", schema).as("data"))

// COMMAND ----------

display(rawKafka)

// COMMAND ----------

rawKafka.printSchema

// COMMAND ----------

// Helper function for recursively flattening a complex schema to a wide columnar schema
def flattenSchema(dfSchema: StructType, prefix: String = null) : Array[Column] = {
  dfSchema.fields.flatMap(f => {
    val colName = if (prefix == null) f.name else (prefix + "." + f.name)
    f.dataType match {
      case st: StructType => flattenSchema(st, colName)
      case _ => Array(col(colName))
    }
  })
}

// COMMAND ----------

// MAGIC %md
// MAGIC #### Extract the fields that are desired

// COMMAND ----------

val flatData = rawKafka.select(flattenSchema(rawKafka.schema):_*)
                       .select($"sys_timestamp", 
                               $"wikipedia", 
                               $"isAnonymous", 
                               $"namespace", 
                               $"page",
                               $"isNewPage",
                               $"isRobot",
                               $"delta",
                               $"pageURL",
                               $"city",
                               $"stateProvince",
                               $"country",
                               $"countryCode2",
                               $"countryCode3",
                               unix_timestamp($"timestamp", "yyyy-MM-dd'T'HH:mm:ss.SSSX").cast("timestamp").as("timestamp"), 
                               $"user")

// COMMAND ----------

// MAGIC %md
// MAGIC #### Alternative method for extracting data from the nested json schema
// MAGIC Not used - purely for demonstration purposes

// COMMAND ----------

// Alternative way to get the same data
val altFlatData = rawKafka
  .select(
    $"sys_timestamp",
    $"data.wikipedia".as("wikipedia"),
    $"data.isAnonymous".as("isAnonymous"),
    $"data.namespace".as("namespace"),
    $"data.page".as("page"),
    $"data.isNewPage".as("isNewPage"),
    $"data.isRobot".as("isRobot"),
    $"data.delta".as("delta"),
    $"data.pageURL".as("pageURL"),
    $"data.geocoding.city".as("city"),
    $"data.geocoding.stateProvince".as("stateProvince"),
    $"data.geocoding.country".as("country"),
    $"data.geocoding.countryCode2".as("countryCode2"),
    $"data.geocoding.countryCode3".as("countryCode3"),
    unix_timestamp($"data.timestamp", "yyyy-MM-dd'T'HH:mm:ss.SSSX").cast("timestamp").as("timestamp"),
    $"data.user".as("user")  
  )

// COMMAND ----------

// MAGIC %md
// MAGIC #### Dump the raw data to Delta
// MAGIC This is going to allow us to:
// MAGIC 1. Have a 'safe store' of the data in a raw format without aggregations placed on it
// MAGIC 2. Use the data for multiple use cases in the future

// COMMAND ----------

val deltaRawLocation = "/ben/demo/delta/wikipedia"

val deltaStream = 
  flatData
    .writeStream
    .format("delta")
    .trigger(Trigger.ProcessingTime("10 seconds"))
    .option("mergeSchema", "true")
    .option("checkpointLocation", deltaRawLocation + "/_checkpoint")
    .outputMode("append")
    .start(deltaRawLocation)

// COMMAND ----------

display(dbutils.fs.ls(deltaRawLocation))

// COMMAND ----------

// MAGIC %md #####Run an optimize and a vacuum command on any older data.

// COMMAND ----------

spark.sql("OPTIMIZE '/ben/demo/delta/wikipedia'")
spark.sql("VACUUM '/ben/demo/delta/wikipedia'")

// COMMAND ----------

// MAGIC %md
// MAGIC #### Read from the raw data source and perform a first-level aggregation
// MAGIC - This first pass aggregation is to get the number of edits and the amount of edit (both additions and deletions) that each user performs in a 5-minute window
// MAGIC - We will then write this data on a 5-minute watermarked window out to another delta table.

// COMMAND ----------

val deltaRead = spark
  .readStream
  .format("delta")
  .load(deltaRawLocation)
  .filter($"isRobot" === false)
  .filter($"namespace" === "article")
  .filter($"delta" =!= 0)
  .withWatermark("timestamp", "5 minutes")
  .groupBy($"user", window($"timestamp", "5 minutes"))
    .agg(sum("delta").as("deltaEditsSum"), count("delta").as("editCount"))
  

// COMMAND ----------

// MAGIC %md
// MAGIC See what the data that we're going to be writing looks like...

// COMMAND ----------

display(deltaRead)

// COMMAND ----------

// MAGIC %md
// MAGIC #### Write the aggregated data to a delta table so that we can use it to predict whether someone is behaving like a 'normal editor' or abnormally
// MAGIC We will be using unsupervised learning to accomplish this (KMeans model) by using the Pipelines API.

// COMMAND ----------

val modelDataDelta = "/ben/demo/delta/wikipedia_user_edits"

val deltaWrite = deltaRead
  .writeStream
  .format("delta")
  .trigger(Trigger.ProcessingTime("5 minutes"))
  .option("mergeSchema", "true")
  .option("checkpointLocation", modelDataDelta + "/_checkpoint")
  .outputMode("append")
  .start(modelDataDelta)

// COMMAND ----------

import org.apache.spark.ml.feature.{OneHotEncoderEstimator, VectorAssembler, VectorSizeHint, Bucketizer}
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.evaluation.ClusteringEvaluator
import org.apache.spark.ml.Pipeline

// COMMAND ----------

// MAGIC %md
// MAGIC #### Get some training data to create a model (static data)
// MAGIC In practice, there would be quite a bit more rigor about selecting training data and ensuring that thresholds would be set according to variance of the data within specified time windows and detection of abnormal behavior flag set points.  This is just for demo purposes of structured streaming, delta, and stream predictions.

// COMMAND ----------

val deltaModelSource = spark
  .read
  .format("delta")
  .load(modelDataDelta)
  .filter($"window.start" >= lit("2018-06-11 16:00:00.000+0000") && ($"window.end" <= lit("2018-06-12 16:00:00:00.000+0000")))

// COMMAND ----------

// Get a count on the full streaming table
val deltaRawCounts = spark.read.format("delta").load(modelDataDelta).count()
print(s"The number of rows I now have is $deltaRawCounts" + "\n")

// COMMAND ----------

deltaModelSource.count

// COMMAND ----------

// MAGIC %md
// MAGIC #### Set up the ML Pipeline flow
// MAGIC 1. Create a discretizer (bucketizer) to group edit quantity behavior into discrete buckets
// MAGIC 2. Assemble the vector for editCount quantity in the 5 minute window and the bucketized edit quantity (number of lines added/deleted from a wikipedia article)
// MAGIC 3. Initialize the KMeans algorithm, setting K=10 (arbitrary for the purposes of this demo)
// MAGIC 4. Create the pipeline object with the stages to run-through for the DataFrame
// MAGIC 5. Build the model

// COMMAND ----------

val bucketSplitsEditSum = Array(Double.NegativeInfinity, -500, -50, 0, 50, 500, Double.PositiveInfinity)
val bucketSplitsEditCounts = Array(0, 1, 5, 10, Double.PositiveInfinity)
val bucketizerEditSum = new Bucketizer()
  .setInputCol("deltaEditsSum")
  .setOutputCol("bucketedEditSum")
  .setSplits(bucketSplitsEditSum)

val bucketizerEditCounts = new Bucketizer()
  .setInputCol("editCount")
  .setOutputCol("bucketedEditCount")
  .setSplits(bucketSplitsEditCounts)

val vectorAssembler = new VectorAssembler()
  .setInputCols(Array("bucketedEditSum", "bucketedEditCount"))
  .setOutputCol("features")

val kMeans = new KMeans().setK(9).setSeed(1L)

val pipeline = new Pipeline()
  .setStages(Array(bucketizerEditSum, bucketizerEditCounts, vectorAssembler, kMeans))

val pipelineModel = pipeline.fit(deltaModelSource)

// COMMAND ----------

// MAGIC %md
// MAGIC #### Visualize the predicted values from the model to see what is getting grouped together
// MAGIC - We're interested in abnormal behavior, and it seems that high edit counts might be suspect, as well as high amounts of single deletions.
// MAGIC - The first display below is the graphical representation of the clusters on the training data set
// MAGIC - The second display is the aggregate values of the predictions in table format.

// COMMAND ----------

display(pipelineModel.transform(deltaModelSource).filter($"deltaEditsSum" < 20000))

// COMMAND ----------

// MAGIC %md
// MAGIC #### Create an ontology mapping for the k-groups based on the model to create human-readable indications of abnormal behavior
// MAGIC - use a udf for the mapping based on the results of the model
// MAGIC - Apply this udf to streaming later on for grouping / alerting

// COMMAND ----------

val groupAssignments = pipelineModel.transform(deltaModelSource).groupBy("prediction").agg(avg($"editCount").alias("actions"), avg("deltaEditsSum").alias("magnitude")).orderBy($"magnitude".asc)
val ver = groupAssignments.rdd.map{x => (x.get(0).asInstanceOf[Integer], x.get(1).asInstanceOf[Double], x.get(2).asInstanceOf[Double])}.collect().toSeq
val highestAddGroup = ver.sortWith(_._3 > _._3)(0)._1
val highestDeleteGroup = ver.sortWith(_._3 < _._3)(0)._1

// COMMAND ----------

def flagActivity(highAddGroup: Integer, highDeleteGroup: Integer) = udf((group: Integer) => group match {
  case `highAddGroup` => 1
  case `highDeleteGroup` => 1
  case _ => 0
})

// COMMAND ----------

val verification = groupAssignments.withColumn("flag", flagActivity(highestAddGroup, highestDeleteGroup)($"prediction"))
display(verification)

// COMMAND ----------

// MAGIC %md 
// MAGIC Get a report on the mapped values and the k-cluster centroids

// COMMAND ----------

// MAGIC %md
// MAGIC #### Open the streaming full data and perform realtime predictions based on the model that we just built

// COMMAND ----------

val realTimeData = spark
  .readStream
  .format("delta")
  .load(modelDataDelta)

// COMMAND ----------

// MAGIC %md 
// MAGIC #### Now with a trained model, let's apply it to the real-time stream
// MAGIC - We're going to be adding in the udfs above to the stream after the real-time prediction
// MAGIC - After the final transformations, save it to another delta table.

// COMMAND ----------

val streamingTransform = pipelineModel
  .transform(realTimeData)
  .withColumn("flag", flagActivity(highestAddGroup, highestDeleteGroup)($"prediction"))
  .select($"user", 
          $"window.start".as("startTime"), 
          $"window.end".as("endTime"), 
          $"deltaEditsSum", 
          $"editCount",
          $"prediction".as("kGroup"),
          $"flag")

val modelDeltaLocation = "/ben/demo/delta/wikipedia_model_kmeans_v3"

val modelStreamTransform = 
  streamingTransform
    .writeStream
    .format("delta")
    .trigger(Trigger.ProcessingTime("5 minutes"))
    .option("mergeSchema", "true")
    .option("checkpointLocation", modelDeltaLocation + "/_checkpoint")
    .outputMode("append")
    .start(modelDeltaLocation)

// COMMAND ----------

// MAGIC %md
// MAGIC #### Read in from the real-time prediction delta table and get real-time (batched) insights into problematic users

// COMMAND ----------

Thread.sleep(5 * 1000)
val realTimePred = 
  spark
    .readStream
    .format("delta")
    .load(modelDeltaLocation)

val realTimePredGrouping = realTimePred
    .groupBy($"flag", $"kGroup")
    .agg(avg($"deltaEditsSum"), avg($"editCount"), count($"user"))

display(realTimePredGrouping)

// COMMAND ----------

// MAGIC %md 
// MAGIC #### Show the distribution of flagged activities

// COMMAND ----------

display(realTimePred)

// COMMAND ----------

// MAGIC %md
// MAGIC #### Show the batched 'problem users' who seem to be abusing the system

// COMMAND ----------

display(realTimePred.filter($"flag" === 1).groupBy($"user").agg(count($"user").as("flaggedTimeGroups"), sum($"deltaEditsSum").as("TotalEditDeltas"), sum($"editCount").as("TotalEditSubmissions")).orderBy($"flaggedTimeGroups".desc))

// COMMAND ----------

// MAGIC %md 
// MAGIC #### Looks like it identified the Wikipedia Bot ClueBog Ng (automated abuse resolution system)!
// MAGIC https://en.wikipedia.org/wiki/Wikipedia:Bots/Requests_for_approval/ClueBot_NG
// MAGIC ##### It also identifies Jon Kolbert, a gentleman who has created a bot to cleanup wikipedia namespaces
// MAGIC https://simple.wikipedia.org/wiki/User_talk:Jon_Kolbert

// COMMAND ----------

// MAGIC %md
// MAGIC #### Even though this is a streaming table, we can still use it exactly like a traditional RDBMS

// COMMAND ----------

val lastHourData = spark.read.format("delta").load(modelDeltaLocation).filter(unix_timestamp($"startTime") > (unix_timestamp(current_timestamp()) - 60 * 60))

// COMMAND ----------

display(lastHourData.filter($"flag" === 1).groupBy($"user").agg(count($"user").as("flaggedTimeGroups"), sum($"deltaEditsSum").as("TotalEditDeltas"), sum($"editCount").as("TotalEditSubmissions")).orderBy($"flaggedTimeGroups".desc))

// COMMAND ----------

// MAGIC %md
// MAGIC #### Now let's see the n hour of data on the stream

// COMMAND ----------

def getLastNHours(streamDF: DataFrame, hoursBack: Integer, flagCutoff: Integer) = {
  streamDF.filter(unix_timestamp($"startTime") > (unix_timestamp(current_timestamp()) - hoursBack * 3600))
    .filter($"flag" === 1)
    .groupBy($"user")
    .agg(count($"user").as("flaggedTimeGroups"), sum($"deltaEditsSum").as("TotalEditDeltas"), sum($"editCount").as("TotalEditSubmissions"))
    .orderBy($"flaggedTimeGroups".desc)
    .filter($"flaggedTimeGroups" >= flagCutoff)
}

// COMMAND ----------

// last hour
display(getLastNHours(realTimePred, 2, 3))

// COMMAND ----------

// last 4 hours
display(getLastNHours(realTimePred, 4, 10))

// COMMAND ----------

// MAGIC %md 
// MAGIC ### This is all well and good, but what happens as this delta table grows?
// MAGIC - Eventually, query speed will suffer due to the number of part files that delta needs to retrieve.
// MAGIC - The way to handle this is to use the OPTIMIZE command

// COMMAND ----------

// Get a count of the delta inventory before optimize
val fileListing = dbutils.fs.ls(modelDeltaLocation).toDF
fileListing.count

// COMMAND ----------

display(fileListing.orderBy($"size".desc))

// COMMAND ----------

// MAGIC %sql OPTIMIZE '/ben/demo/delta/wikipedia_model_kmeans_v3' 

// COMMAND ----------

val fileListingAfter = dbutils.fs.ls(modelDeltaLocation).toDF
fileListingAfter.count

// COMMAND ----------

// MAGIC %md
// MAGIC #### The vacuum command will cleanup files older than 7 days.  It is not advisable to do periods where late-arriving data might not find its target part file.
// MAGIC Exercise extreme caution with submitting a RETAIN modifier too close to current time (e.g. VACUUM '/path/to/data' RETAIN 2 HOURS)

// COMMAND ----------

// MAGIC %sql VACUUM '/ben/demo/delta/wikipedia_model_kmeans_v3' RETAIN 200 HOURS

// COMMAND ----------

val fileListingAfterVac = dbutils.fs.ls(modelDeltaLocation).toDF
fileListingAfterVac.count

// COMMAND ----------

