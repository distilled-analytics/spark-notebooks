// Databricks notebook source
// MAGIC %md
// MAGIC The logic of this notebook is derived from "KafkaToDelta", provided by Ben Wilson of Databricks

// COMMAND ----------

// Import packages
import org.apache.spark.sql.functions._ 
import org.apache.spark.sql.types._
import org.apache.spark.sql.streaming._
import org.apache.spark.sql.avro._ // <-- from_avro() lives here since Databricks 5.2
import org.apache.spark.sql._
import java.net.{InetAddress, Socket}
import scala.io.Source

// COMMAND ----------

// Global setting taken straight from "KafkaToDelta". 
// As far as I remember, it should be equal to (or twice?) the number of CPU cores in the cluster.
spark.conf.set("spark.sql.shuffle.partitions", 8)

// COMMAND ----------

// Some globals:
// Kafka Server address
val kafkaServer       = "18.217.133.99:9092"
// Avro schema registry service URL
val schemaRegistryURL = "http://18.191.132.207:8081"

// COMMAND ----------

// Get the notebook context.
// As we share this notebook among several developers, there will be multiple sessions running this notebook streams. 
//   To avoid multiple session writing to the same delta tables, it is necessary to isolate the Delta output file directory for each session.
//   Here we derive the hme path from user name who owns the session and notebook path. This should guarantie the uniquness of the path. 
val notebookCtx  = dbutils.notebook.getContext()
val notebookPath = notebookCtx.notebookPath.get
val user         = notebookCtx.tags("user")

// All output for a session will be under this directory:
val homeDir = s"/mnt/sandbox/${user}${notebookPath}"
// Any Delta directories should be under this path:
val deltaDir = s"${homeDir}/delta" 


// COMMAND ----------

// Helper function for recursively flattening a complex schema to a wide columnar schema
def flattenSchema(dfSchema: StructType, prefix: String = null) : Array[Column] = {
  dfSchema.fields.flatMap(f => {
    val colName = if (prefix == null) f.name else (prefix + "." + f.name)
    f.dataType match {
      case st: StructType => flattenSchema(st, colName)
      case _ => Array(col(colName))
    }
  })
}

// COMMAND ----------


// The Mobile SDK collects data into 5 or more topics.

// We take only one topic here as an example. 
val topicName          = "damobilesdk-GpsMetric"

// This topic has a "collection" ("event") timestamp field with the following name.
//   The value is the time when the source record was collected at the mobile device.
val timestampFieldName = "lCollectionTimeUtc"

// Establish the topic's stream read from the Kafka server. 
// The topic key and value are stored in Avro format. We decode both with from_avro() helper. 
//   That gives us a "structure-type" field named "value", i.e. a nested record. 
//   The schema of that record comes from Kafka schema registry service.
// We also rename the Kakfka timestamp that relates to the point of data entry into Kafka.
val rawKafka = spark.readStream
  .format("kafka")
  .option("kafka.bootstrap.servers", kafkaServer)
  .option("failOnDataLoss",          "false")
  .option("subscribe",               topicName)
  .load()
  .select(
      // Kafka standard header fields:
      $"topic",
      $"partition",
      $"offset",
      $"timestamp".as("timestamp_kafka"), // timestamp from Kafka (when received by Kafka topic)
      $"timestampType",
      // Topic Key: ( field name, topic name, schema registry URL)
      from_avro($"key",   s"${topicName}-key",   schemaRegistryURL).as("key"),
      // Topic Value (a structure) ( field name, topic name, schema registry URL)
      from_avro($"value", s"${topicName}-value", schemaRegistryURL).as("value"))


// COMMAND ----------

// Print the schema of what we have:
rawKafka.printSchema()

// COMMAND ----------

// This will start a stream and will just display the realtime data ingestion graphs.
display(rawKafka)

// COMMAND ----------

Thread.sleep(10000)

// COMMAND ----------

val tileSize = 0.001 //math.pow(10., 3)

// A transform of the rawKafka streaming DataFrame.

// - Flatten schema.
// - Add a unified timestamp field named also "timestamp" by converting the topic-specific "collection timestamp".
// - Add lat/lon tile numbers, just for example.
val flatData = rawKafka.select(flattenSchema(rawKafka.schema):_*)
                       .select($"*",
                               col(s"${timestampFieldName}").cast("timestamp").as("timestamp"), // Timestamp from Mobile SDK (when collected by Mobile SDK)
                               floor($"dLatitude" /0.001).as("lat_tile"),
                               floor($"dLongitude"/0.001).as("lng_tile"))


// COMMAND ----------

// MAGIC %md
// MAGIC Dump the (flattened) raw data received from Kafka, to Delta
// MAGIC This is going to allow us to:
// MAGIC 1. Have a 'safe store' of the data in a raw format without aggregations placed on it
// MAGIC 2. Use the data for multiple use cases in the future

// COMMAND ----------

// Raw data will be stored as Delta under this path:
val deltaRawTopicPath  = s"${deltaDir}/${topicName}"

val deltaStream = 
  flatData
    .writeStream
    .format("delta")
    .trigger(Trigger.ProcessingTime("10 seconds"))
    .option("mergeSchema", "true")
    .option("checkpointLocation", s"${deltaRawTopicPath}/_checkpoint")
    .outputMode("append")
    .start(deltaRawTopicPath)


// COMMAND ----------

Thread.sleep(10000)

// COMMAND ----------

// Check the path where we store our raw data from Kafka
display(dbutils.fs.ls(deltaRawTopicPath))

// COMMAND ----------

// Run an optimize and a vacuum command on any older data:
spark.sql(s"OPTIMIZE '${deltaRawTopicPath}'")
spark.sql(s"VACUUM '${deltaRawTopicPath}'")

// COMMAND ----------

// MAGIC %md
// MAGIC Read from the raw data source and perform a first-level aggregation
// MAGIC - This first pass aggregation is to get the number of GPS records that Mobile SDK reported for each user performs in a 5-minute window
// MAGIC - We will then write this data on a 5-minute watermarked window out to another delta table.

// COMMAND ----------

val deltaReadUsers = spark
  .readStream
  .format("delta")
  .load(deltaRawTopicPath)
  .withWatermark("timestamp", "5 minutes")
  .groupBy($"lUserGlobalID", window($"timestamp", "5 minutes"))
    .agg(count("*").as("gpsRecordCount"))


// COMMAND ----------

// Start the "monitoring" stream
display(deltaReadUsers)

// COMMAND ----------

Thread.sleep(10000)

// COMMAND ----------

/*
val deltaReadTiles = spark
  .readStream
  .format("delta")
  .load(deltaRawTopicPath)
  .withWatermark("timestamp", "5 minutes")
  .groupBy($"lat_tile", $"lng_tile", window($"timestamp", "5 minutes"))
    .agg(count("*").as("gpsRecordCount"))
  */

// COMMAND ----------

//display(deltaReadTiles)

// COMMAND ----------

// MAGIC %md
// MAGIC Write the aggregated data to another delta table

// COMMAND ----------

// This is the path to "aggregared" data 
val deltaUsersModelPath = s"${deltaRawTopicPath}_user_counts"

// This will start the stream
val deltaWriteUsers = deltaReadUsers
  .writeStream
  .format("delta")
  .trigger(Trigger.ProcessingTime("5 minutes"))
  .option("mergeSchema", "true")
  .option("checkpointLocation", s"${deltaUsersModelPath}/_checkpoint")
  .outputMode("append")
  .start(deltaUsersModelPath)


// COMMAND ----------

Thread.sleep(10000)

// COMMAND ----------

/*
val deltaTilesModelPath = s"${deltaRawTopicPath}_tile_counts"

val deltaWriteTiles = deltaReadTiles
  .writeStream
  .format("delta")
  .trigger(Trigger.ProcessingTime("5 minutes"))
  .option("mergeSchema", "true")
  .option("checkpointLocation", s"{deltaTilesModelPath}/_checkpoint")
  .outputMode("append")
  .start(deltaTilesModelPath)
*/


// COMMAND ----------

// Get a count of the delta inventory
val fileListing = dbutils.fs.ls(deltaUsersModelPath).toDF
fileListing.count

// COMMAND ----------

// Display file table
display(fileListing.orderBy($"size".desc))

// COMMAND ----------

// MAGIC %md
// MAGIC The remaining logic from "KafkaToDelta" is removed.